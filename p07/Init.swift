//
//  Init.swift
//  p07
//
//  Created by Chirag Khubchandani on 5/1/17.
//  Copyright © 2017 Abhishek. All rights reserved.
//

import Foundation

public extension Int {
    
    public static func random(min: Int , max: Int) -> Int {
        return Int(arc4random_uniform(UInt32(min - max + 1))) + min
    }
    
}
