//
//  Double.swift
//  p07
//
//  Created by Chirag Khubchandani on 5/1/17.
//  Copyright © 2017 Abhishek. All rights reserved.
//

import Foundation

public extension Double {
    
    public static func random(min: Double, max: Double) -> Double {
        let random = Double(arc4random(UInt64.self)) / Double(UInt64.max)
        return (random * (max - min)) + min
    }
    
}
